/*
Copyright © 2016, The Pennsylvania State University
All rights reserved.

Copyright © 2016 Forschungszentrum Jülich GmbH
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted under the GNU General Public License v3 and provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

Disclaimer
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

You should have received the GNU GENERAL PUBLIC LICENSE v3 with this file in license.txt but can also be found at http://www.gnu.org/licenses/gpl-3.0.en.html

NOTE: The GPL.v3 license requires that all derivative work is distributed under the same license. That means that if you use this source code in any other program, you can only distribute that program with the full source code included and licensed under a GPL license.

 */

#include "GrowthImpedance.hpp"
#include <cmath>
#include "../PlantType.hpp"

// The one static instance of impedanceCalculator.
// Will be instantiated on first use (but deallocated never, yeah?)
//
// TODO separate pointer for RootGrowthDirectionImpedance is basically just to
// avoid writing out `RootGrowthImpedanceRateMultiplier::impedanceCalculator`
// every time I reference it inside RootGrowthDirectionImpedance.
// This is silly and should be fixed by giving these classes an inheritance
// relationship instead of just friendship.
SimulaBase* RootGrowthImpedanceRateMultiplier::impedanceCalculator = nullptr;
SimulaBase* RootGrowthDirectionImpedance::impedanceCalculator = RootGrowthImpedanceRateMultiplier::impedanceCalculator;

RootGrowthImpedanceRateMultiplier::RootGrowthImpedanceRateMultiplier(SimulaDynamic* pSD):DerivativeBase(pSD){
	if(!impedanceCalculator){
		impedanceCalculator = ORIGIN->getPath("/environment/soil/soilPenetrationResistance", "kPa");
	}

	std::string plantType;
	PLANTTYPE(plantType, pSD);
	std::string rootType;
	int pos = 3;
	if (pSD->getParent()->getName()=="growthpoint") { pos = 2; }
	pSD->getParent(pos)->getChild("rootType")->get(rootType);
	SimulaBase* p(GETROOTPARAMETERS(plantType, rootType));
	halfGrowthImpedance = p->existingChild("soilImpedanceFor50PercentGrowthSlowdown", "kPa");
	if (!halfGrowthImpedance) {
		halfGrowthImpedance = pSD->getChild("soilImpedanceFor50PercentGrowthSlowdown", "kPa");
	}
}

void RootGrowthImpedanceRateMultiplier::calculate(const Time &t, double &var){
	var = 1;

	// This is a Michaelis-Menten curve whose Km is interpretable as
	// "impedance where growth is reduced to 50% of unimpeded growth"
	// Default Km of 2000 kPa is a guessed average from the few studies
	// I could find, but I did no formal metaanalysis. Don't trust it too much.
	impedanceCalculator->get(pSD, t, var);
	double km = 2000;
	halfGrowthImpedance->get(t, km);
	var = 1.0 - (var/(km+var));
	if (var < 0 || var > 1 || !std::isnormal(var)) {
		msg::error("Numerical problem in RootGrowthImpedanceRateMultiplier: result is "
			+ std::to_string(var) + "but should in [0, 1]");
	}
}

std::string RootGrowthImpedanceRateMultiplier::getName() const{
	return "rootGrowthImpedanceRateMultiplier";
}

DerivativeBase * newInstantiationRootGrowthImpedanceRateMultiplier(SimulaDynamic* const pSD){
   return new RootGrowthImpedanceRateMultiplier(pSD);
}



RootDiameterImpedance::RootDiameterImpedance(SimulaDynamic* pSD):DerivativeBase(pSD){

	lengthImpedance = pSD->getSibling("rootGrowthImpedance");

	std::string plantType;
	PLANTTYPE(plantType, pSD);
	std::string rootType;
	int pos = 3;
	if (pSD->getParent()->getName()=="growthpoint") { pos = 2; }
	pSD->getParent(pos)->getChild("rootType")->get(rootType);
	SimulaBase* p(GETROOTPARAMETERS(plantType, rootType));
	diameterScalingExponent = p->existingChild("scalingExponentForRootDiameterIncreaseFromImpedance");
	if (!diameterScalingExponent) {
		diameterScalingExponent = pSD->getChild("scalingExponentForRootDiameterIncreaseFromImpedance");
	}
}

void RootDiameterImpedance::calculate(const Time &t, double &var){
	double exp = 0.0;
	lengthImpedance->get(t, var);
	diameterScalingExponent->get(t, exp);

	// Scales diameter relative to length impedance (0-1),
	// not directly relative to soil penetration resistance.
	// Two values of exp have special properties:
	// (1) exp = 0.5 => diameter increase offsets length decrease
	// 		=> no change in root volume
	//		=> architecture changes but C sink stays constant
	// (2) exp = 0 => scale = 1
	//		=> diameter unaffected by impedance
	var = 1.0 / pow(var, exp);
}

std::string RootDiameterImpedance::getName() const{
	return "rootDiameterImpedanceMultiplier";
}

DerivativeBase * newInstantiationRootDiameterImpedance(SimulaDynamic* const pSD){
   return new RootDiameterImpedance(pSD);
}



RootGrowthDirectionImpedance::RootGrowthDirectionImpedance(SimulaDynamic* pSD):DerivativeBase(pSD){
	if(!impedanceCalculator){
		impedanceCalculator = ORIGIN->getPath("/environment/soil/soilPenetrationResistance", "kPa");
	}
	growthPoint = dynamic_cast<SimulaPoint*>(pSD->getParent());
}


void RootGrowthDirectionImpedance::calculate(const Time &t, Coordinate &vec){
	vec.x = 0;
	vec.y = 0;
	vec.z = 0;
	pSD->getParent()->getAbsolute(t, position);
	if (position.y >= 0){
		return;
	}
	Coordinate lastDirection;
	const SimulaPoint::Table *table = growthPoint->getTable();
	bool found(false);
	for (SimulaPoint::Table::const_reverse_iterator it(table->rbegin());
			it != table->rend(); ++it) {
		lastDirection = it->second.rate;
		if (vectorlength(lastDirection) > 1E-8) {
			found = true;
			break;
		}
	}
	if (!found) return;
	Coordinate testPosition;
	Coordinate tempvec;
	tempvec.x = 0.0;
	tempvec.y = 0.0;
	tempvec.z = 1.0;
	Coordinate perp = perpendicular(tempvec, lastDirection);
	normalizeVector(perp);
	Coordinate perp2 = perpendicular(perp, lastDirection);
	testPosition = position + lastDirection;
	double temp;
	impedanceCalculator->get(t, testPosition, temp);
	vec = vec + lastDirection*ImpedanceFactor(t, temp);
	for (double axialAngle = 10.0; axialAngle < 85.0; axialAngle += 10.0){
		for (double radialAngle = 0.0; radialAngle < 355.0; radialAngle += 10.0){
			testPosition = position + lastDirection*cos(axialAngle*M_PI/180.0) + perp*sin(axialAngle*M_PI/180.0)*cos(radialAngle*M_PI/180.0) + perp2*sin(axialAngle*M_PI/180.0)*sin(radialAngle*M_PI/180.0);
			if (testPosition.y > 0){
				vec.x = 0;
				vec.y = 0;
				vec.z = 0;
				return;
			}
			double temp;
			impedanceCalculator->get(t, testPosition, temp);
			vec = vec + (lastDirection*cos(axialAngle*M_PI/180.0) + perp*sin(axialAngle*M_PI/180.0)*cos(radialAngle*M_PI/180.0) + perp2*sin(axialAngle*M_PI/180.0)*sin(radialAngle*M_PI/180.0))*ImpedanceFactor(t, temp);
		}
	}
	vec = vec - lastDirection*dotProduct(lastDirection, vec)/dotProduct(lastDirection, lastDirection);
/// TODO The right scaling factor has to be found here, once we've settled on workable units
/// soiImpedance should handle most variable considerations, just need to ensure output is within range
// This scaling factor seems to work pretty okay
	vec = vec*0.01;
//	normalizeVector(vec);
//	bulkDensity->get(t, position, temp);
//	vec = vec*ImpedanceFactor(t, temp);
}

// This should depend on the root class, diameter etc. The shape of the function should also be different.
double RootGrowthDirectionImpedance::ImpedanceFactor(const Time &t, const double &imp){
	if (imp > maxBulkDensity) return 0;
	if (imp < minBulkDensity) return 1;
	return (maxBulkDensity - imp)/(maxBulkDensity - minBulkDensity);
}

std::string RootGrowthDirectionImpedance::getName() const{
	return "rootGrowthDirectionImpedance";
}

DerivativeBase * newInstantiationRootGrowthDirectionImpedance(SimulaDynamic* const pSD){
   return new RootGrowthDirectionImpedance(pSD);
}


//registration of classes
class AutoRegisterGrowthImpedanceInstantiationFunctions {
public:
   AutoRegisterGrowthImpedanceInstantiationFunctions() {
		BaseClassesMap::getDerivativeBaseClasses()["rootGrowthImpedanceRateMultiplier"] = newInstantiationRootGrowthImpedanceRateMultiplier;
		BaseClassesMap::getDerivativeBaseClasses()["rootDiameterImpedanceMultiplier"] = newInstantiationRootDiameterImpedance;
		BaseClassesMap::getDerivativeBaseClasses()["rootGrowthDirectionImpedance"] = newInstantiationRootGrowthDirectionImpedance;
   }
};

static AutoRegisterGrowthImpedanceInstantiationFunctions pgi;
