/*
 Copyright © 2022 Ernst Dirk Schäfer
 All rights reserved.

 Redistribution and use in source and binary forms, with or without modification, are permitted under the GNU General Public License v3 and provided that the following conditions are met:
 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

 Disclaimer
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 You should have received the GNU GENERAL PUBLIC LICENSE v3 with this file in license.txt but can also be found at http://www.gnu.org/licenses/gpl-3.0.en.html

 NOTE: The GPL.v3 license requires that all derivative work is distributed under the same license. That means that if you use this source code in any other program, you can only distribute that program with the full source code included and licensed under a GPL license.

 */


#include "Fertilization.hpp"


Fertilization::Fertilization(const Mesh_base &mesh, const std::string &nutrient)
{
	// Z is the vertical coordinate in this mesh!
	auto &z(mesh.getCordZ());
	numNodes = z.size();
	tVector meshVols = mesh.getvol();

	SimulaBase* p;
	p = ORIGIN->getPath("environment/soil/" + nutrient + "/fertilization");
	rate = p->getChild("fertilizerRate");

	double unitCorrection = 1.;
	if (rate->getUnit() == "g/day"){
		unitCorrection = 1.;
	}
	else if (rate->getUnit() == "g/cm2/day"){
		SimulaBase *probe;
		double area;
		probe = ORIGIN->existingPath("environment/plantingScheme/rowSpacing");
		if (probe){
			double wid, len;
			probe->get(wid);
			probe = ORIGIN->getPath("environment/plantingScheme/inTheRowSpacing");
			probe->get(len);
			area = wid*len;
		} else{
			Coordinate minCorner, maxCorner;
			probe = ORIGIN->getPath("environment/dimensions/minCorner");
			probe->get(minCorner);
			probe = ORIGIN->getPath("environment/dimensions/maxCorner");
			probe->get(maxCorner);
			// Here y is the vertical coordinate!
			area = fabs(minCorner.x - maxCorner.x)*fabs(minCorner.z - maxCorner.z);
		}
		unitCorrection = area;
	} else{
		msg::error("Unknown unit");
	}

	depthDistribution = p->getChild("distribution");
	double temp;
	depthFactor = 0;
	tVector nodeFactors;
	nodeFactors.resize(numNodes);
	for (int i = 0; i != numNodes; ++i) {
		depthDistribution->get(z[i], temp);
		nodeFactors[i] = temp*meshVols[i];
		depthFactor += temp*meshVols[i];
	}
	depthMultipliers.resize(numNodes);
	for (unsigned int i = 0; i < numNodes; ++i){
		depthMultipliers[i] = unitCorrection*nodeFactors[i]/depthFactor;
	}
}

void Fertilization::fertilization(const double & t0, tVector &fertil){
	// output:  fertil
	if(fertil.size()!=numNodes) fertil.resize(numNodes);
	double currentRate;
	rate->get(t0, currentRate);
	fertil = depthMultipliers*currentRate;
}

