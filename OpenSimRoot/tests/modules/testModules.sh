#!/bin/bash
echo Testing modules

mkdir -p testResults
cd testResults

echo Testing OneSimpleStraightRoot.xml 
if [ -z "$1" ] 
then
exe="../../Release/OpenSimRoot"
else
exe="$1"
fi

[ -e ../$exe ] && echo using $exe as exe || { echo exe $exe not found ; exit 1 ; }

../$exe ../OneSimpleStraightRoot.xml > screen.out 2>&1 && echo "Test OneSimpleStraightRoot.xml ran"  || echo "Test OneSimpleStraightRoot.xml failed"
mv tabled_output.tab ResultOneSimpleStraightRoot.tab  2>/dev/null
grep -v "Simulation took (hours:minutes:seconds)" warnings.txt &> WarningsOneSimpleStraightRoot.txt 
mv roots010.00.vtu roots010.00.OneSimpleStraightRoot.vtu  2>/dev/null
mv roots010.00.vtp roots010.00.OneSimpleStraightRoot.vtp  2>/dev/null

../$exe ../PartiallyPredefinedBranchedRoot.xml >> screen.out 2>&1 && echo "Test PartiallyPredefinedBranchedRoot.xml ran" || echo "Test PartiallyPredefinedBranchedRoot.xml failed"
#[ -e /usr/bin/Rscripts ] && cat tabled_output.tab | grep -v path | ../../../scripts/plot -qo PartiallyPredefinedBranchedRoot
mv tabled_output.tab ResultPartiallyPredefinedBranchedRoot.tab  2>/dev/null
grep -v "Simulation took (hours:minutes:seconds)" warnings.txt &> WarningsPartiallyPredefinedBranchedRoot.txt  
#vti run, but output not in default test as it is 7 mb or so. 
#mv roots_rasterImage_014.00.vti ResultPartiallyPredefinedBranchedRoot.vti 
mv roots014.00.vtp ResultPartiallyPredefinedBranchedRoot.vtp  2>/dev/null

../$exe ../Barber-Cushman.xml >> screen.out 2>&1 && echo "Test Barber-Cushman.xml ran" || echo "Test Barber-Cushman.xml failed"
mv tabled_output.tab ResultBarber-Cushman.tab  2>/dev/null
grep -v "Simulation took (hours:minutes:seconds)" warnings.txt &> WarningsBarber-Cushman.txt  

../$exe ../SimpleCropModel.xml >> screen.out 2>&1 && echo "Test SimpleCropModel.xml ran" || echo "Test SimpleCropModel.xml failed"
mv tabled_output.tab ResultSimpleCropModel.tab  2>/dev/null
grep -v "Simulation took (hours:minutes:seconds)" warnings.txt &> WarningsSimpleCropModel.txt  

../$exe ../straightRootSchnepf.xml >> screen.out 2>&1 && echo "Simulation StraighRootSchnepf ran" || echo "Simulation StraighRootSchnepf failed"
mv roots020.00.vtp ResultWaterTestSchnepf.vtp  2>/dev/null

rm -f *0.vtu
rm -f *0.vtp
rm -f *0.vti
rm -f *.pvd
rm -f warnings.txt
cd ..

./testGravitropism.sh "$exe"
rexe=$?
echo Running water uptake test
./testWaterUptakeSchnepf.R  
rexe=$(($?+$rexe))

echo Done running testing modules, comparing results
#no use as mem use always differs somewhat
#diff -q testResults/screen.out refTestResults/screen.out 

diff -q "testResults/barber_cushman_explicit.tab" "refTestResults/barber_cushman_explicit.tab"
rexe=$?
diff -q "testResults/barber_cushman_ode23.tab" "refTestResults/barber_cushman_ode23.tab"
rexe=$(($?+$rexe))
diff -q "testResults/barber-cushman_dataPoint00000_root.tab" "refTestResults/barber-cushman_dataPoint00000_root.tab"
rexe=$(($?+$rexe))
diff -q "testResults/ResultBarber-Cushman.tab" "refTestResults/ResultBarber-Cushman.tab"
rexe=$(($?+$rexe))
diff -q "testResults/ResultOneSimpleStraightRoot.tab" "refTestResults/ResultOneSimpleStraightRoot.tab"
rexe=$(($?+$rexe))
diff -q "testResults/ResultSimpleCropModel.tab" "refTestResults/ResultSimpleCropModel.tab"
rexe=$(($?+$rexe))

echo Comparing VTU and VTP files, differences in these will not contribute to the error code

diff -q "testResults/ResultPartiallyPredefinedBranchedRoot.vtp" "refTestResults/ResultPartiallyPredefinedBranchedRoot.vtp"
diff -q "testResults/roots010.00.OneSimpleStraightRoot.vtp" "refTestResults/roots010.00.OneSimpleStraightRoot.vtp"
diff -q "testResults/roots010.00.OneSimpleStraightRoot.vtu" "refTestResults/roots010.00.OneSimpleStraightRoot.vtu"
diff -q "testResults/ResultPartiallyPredefinedBranchedRoot.vtp" "refTestResults/ResultPartiallyPredefinedBranchedRoot.vtp"

echo Done exiting with code $rexe
exit $rexe

